import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'mx.lania.tsadm2023.appcapacitormenu',
  appName: 'AppCapacitorMenu',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
