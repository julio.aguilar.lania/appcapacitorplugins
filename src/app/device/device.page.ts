import { Component, OnInit } from '@angular/core';
import { BatteryInfo, Device, DeviceInfo } from '@capacitor/device'

@Component({
  selector: 'app-device',
  templateUrl: './device.page.html',
  styleUrls: ['./device.page.scss'],
})
export class DevicePage implements OnInit {
  infoDispositivo:DeviceInfo | any = {}
  infoBateria:BatteryInfo | any = {}

  constructor() { }

  ngOnInit() {
  }

  ionViewDidEnter() {
    Device.getInfo()
      .then(info => { this.infoDispositivo = info})
    Device.getBatteryInfo()
      .then(bInfo => { this.infoBateria = bInfo})
  }

}
