import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Geolocation } from '@capacitor/geolocation';
import { Position } from '@capacitor/geolocation/dist/esm/definitions';
import { GoogleMap } from '@capacitor/google-maps';

@Component({
  selector: 'app-location',
  templateUrl: './location.page.html',
  styleUrls: ['./location.page.scss'],
})
export class LocationPage implements OnInit {
  permisoLocation: string = ''
  permisoCoarseLocation: string = ''
  posicion:Position | any = {}

  @ViewChild('map')
  mapRef: ElementRef<HTMLElement> | undefined;
  newMap: GoogleMap | undefined;

  constructor() { }

  ngOnInit() {
    Geolocation.checkPermissions()
      .then(ps => {
        this.permisoLocation = ps.location;
        this.permisoCoarseLocation = ps.coarseLocation;
        this.obtenerPosicion();
      })
  }

  pedirPermiso() {
    console.log('GEOLOCATION: Pidiendo permiso')
    Geolocation.requestPermissions({permissions:['location','coarseLocation']})
      .then(ps => {
        this.permisoLocation = this.permisoLocation + ', ' + ps.location;
        this.permisoCoarseLocation = this.permisoCoarseLocation + ', ' + ps.coarseLocation;
        this.obtenerPosicion();
      })
    //ERROR: this.obtenerPosicion();
  }

  obtenerPosicion() {
    console.log('GEOLOCATION: obtenerPosicion')
    if (this.permisoLocation === 'granted' || this.permisoCoarseLocation === 'granted') {
      console.log('granted')
      Geolocation.getCurrentPosition()
        .then(pos => {
          console.log(pos)
          this.posicion = pos
        })
    }
  }

  createMap() {
    if (this.mapRef) {
      GoogleMap.create({
        id:'miMapa',
        element: this.mapRef.nativeElement,
        apiKey:'AIzaSyDCpbKtVl8PeJonGHhi2BIeX77Adl9CpG0',
        config:{
          center: {
            lat: 19.51979251363248,
            lng:-96.91534337127159
          },
          zoom: 8,
        }
      }).then(map => {this.newMap = map})
    }
  }
}
