import { Component, OnInit } from '@angular/core';
import { Camera } from '@capacitor/camera';
import { CameraResultType } from '@capacitor/camera/dist/esm/definitions';

@Component({
  selector: 'app-camara',
  templateUrl: './camara.page.html',
  styleUrls: ['./camara.page.scss'],
})
export class CamaraPage implements OnInit {
  permisoCamara: string = ''
  permisoFotos: string = ''

  constructor() { }

  ngOnInit() {
    Camera.checkPermissions()
      .then(ps => {
        this.permisoCamara = ps.camera;
        this.permisoFotos = ps.photos;
      })
  }

  pedirPermiso() {
    console.log('CAMERA: Pidiendo permiso')
    Camera.requestPermissions({permissions:['camera']})
      .then(ps => { this.permisoCamara = this.permisoCamara + ', ' + ps.camera})
  }

  pedirPermisoFotos() {
    console.log('FOTOS: Pidiendo permiso')
    Camera.requestPermissions({permissions:['photos']})
      .then(ps => { this.permisoFotos = this.permisoFotos + ', ' + ps.photos})
  }

  tomarFoto() {
    console.log('CAMERA: Tomando foto')
    Camera.getPhoto({resultType:CameraResultType.Uri})
      .then(res => { console.log(res)})
  }

}
