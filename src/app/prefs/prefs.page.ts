import { Component, OnInit } from '@angular/core';
import { Preferences } from '@capacitor/preferences';

@Component({
  selector: 'app-prefs',
  templateUrl: './prefs.page.html',
  styleUrls: ['./prefs.page.scss'],
})
export class PrefsPage implements OnInit {
  NOMBRE_LLAVE = 'APPCAPACITOR_LLAVE_UNO'
  valor: string = 'valortemporal'

  constructor() { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    Preferences.get({key:this.NOMBRE_LLAVE})
      .then(v => {if (v.value) this.valor = v.value})
  }

  guardarFecha() {
    let fecha = new Date().toISOString()
    Preferences.set({key:this.NOMBRE_LLAVE, value: fecha})
      .then(res => {})
  }

}
