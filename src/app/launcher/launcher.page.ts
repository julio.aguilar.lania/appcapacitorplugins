import { Component, OnInit } from '@angular/core';
import { AppLauncher } from '@capacitor/app-launcher';

interface App {
  nombre:string,
  paquete:string,
  url:string,
  puedeAbrir:boolean
}

@Component({
  selector: 'app-launcher',
  templateUrl: './launcher.page.html',
  styleUrls: ['./launcher.page.scss'],
})
export class LauncherPage implements OnInit {
  listaApps: App[] = [
    {nombre:'YouTube',paquete:'com.google.android.youtube',url:'https://www.youtube.com/watch?v=i6Jk9M6xv4g', puedeAbrir:false},
    {nombre:'Teams', paquete:'com.microsoft.teams',url:'https://teams.microsoft.com/l/meetup-join/19%3ameeting_MDdlMGU0OWEtNWVlYy00ODhiLWI4YTQtMGEyZjNiNDk4N2Ez%40thread.v2/0?context=%7b%22Tid%22%3a%227c040521-7bb5-4b73-9974-d049159d81ae%22%2c%22Oid%22%3a%22ef4b8a56-46fe-4083-9593-ece5d6d80e72%22%7d', puedeAbrir:false}
  ]

  constructor() { }

  ngOnInit() {
    this.listaApps.forEach(app => {
      AppLauncher.canOpenUrl({url:app.paquete})
        .then(res => {app.puedeAbrir = res.value})
    })
  }

  abrirUrl(path:string) {
    AppLauncher.openUrl({url:path})
      .then(res=>{})
  }

}
